package org.phoenixdb.tools;

import java.io.InputStream;

public interface CommandLineInterpreter {

    void run(String resourceName, InputStream in) throws Exception;

}
