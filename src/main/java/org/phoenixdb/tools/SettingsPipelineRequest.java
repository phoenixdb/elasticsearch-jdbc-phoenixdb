package org.phoenixdb.tools;

import org.elasticsearch.common.settings.Settings;
import org.phoenixdb.pipeline.PipelineRequest;

public class SettingsPipelineRequest implements PipelineRequest<Settings> {

    private Settings settings;

    @Override
    public Settings get() {
        return settings;
    }

    @Override
    public SettingsPipelineRequest set(Settings settings) {
        this.settings = settings;
        return this;
    }

    @Override
    public String toString() {
        return settings.getAsMap().toString();
    }
}