package org.phoenixdb.tools;

import java.io.FileInputStream;
import java.io.InputStream;

public class Runner {

    public static void main(String[] args) {
        try {
            Class clazz = Class.forName(args[0]);
            CommandLineInterpreter commandLineInterpreter = (CommandLineInterpreter) clazz.newInstance();
            InputStream in = args.length > 1 ? new FileInputStream(args[1]) : System.in;
            commandLineInterpreter.run("args", in);
            in.close();
        } catch (Throwable e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.exit(0);
    }

}
