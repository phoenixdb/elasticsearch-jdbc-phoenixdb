
package org.phoenixdb.elasticsearch.common.util;

import org.phoenixdb.elasticsearch.jdbc.strategy.Sink;

import java.io.IOException;

/**
 * This class consumes pairs from a key/value stream
 * and transports them to the sink.
 */
public class SinkKeyValueStreamListener<K, V> extends PlainKeyValueStreamListener<K, V> {

    private Sink output;

    public SinkKeyValueStreamListener<K, V> output(Sink output) {
        this.output = output;
        return this;
    }

    public SinkKeyValueStreamListener<K, V> shouldIgnoreNull(boolean shouldIgnoreNull) {
        super.shouldIgnoreNull(shouldIgnoreNull);
        return this;
    }

    public SinkKeyValueStreamListener<K, V> shouldDetectGeo(boolean shouldDetectGeo) {
        super.shouldDetectGeo(shouldDetectGeo);
        return this;
    }

    public SinkKeyValueStreamListener<K, V> shouldDetectJson(boolean shouldDetectJson) {
        super.shouldDetectJson(shouldDetectJson);
        return this;
    }

    /**
     * The object is complete. Push it to the sink.
     *
     * @param object the object
     * @return this value listener
     * @throws java.io.IOException if this method fails
     */
    public SinkKeyValueStreamListener<K, V> end(IndexableObject object) throws IOException {
        if (object.isEmpty()) {
            return this;
        }
        if (output != null) {
            if (object.optype() == null) {
                output.index(object, false);
            } else if ("index".equals(object.optype())) {
                output.index(object, false);
            } else if ("create".equals(object.optype())) {
                output.index(object, true);
            } else if ("update".equals(object.optype())) {
                output.update(object);
            } else if ("delete".equals(object.optype())) {
                output.delete(object);
            } else {
                throw new IllegalArgumentException("unknown optype: " + object.optype());
            }
        }
        return this;
    }

}
