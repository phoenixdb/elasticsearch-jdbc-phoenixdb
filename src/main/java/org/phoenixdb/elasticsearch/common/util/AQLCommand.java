
package org.phoenixdb.elasticsearch.common.util;

import org.elasticsearch.common.io.Streams;
import org.elasticsearch.common.xcontent.support.XContentMapValues;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * The AQL command
 */
public class AQLCommand {

    private String aql;

    private static final Pattern STATEMENT_PATTERN = Pattern.compile("^\\s*(update|insert)", Pattern.CASE_INSENSITIVE);

    private List<Object> params = new LinkedList<>();

    private boolean write;

    private Map<String, Object> register = new HashMap<>();

    private boolean callable;

    public AQLCommand setAQL(String aql) throws IOException {
        if (aql.endsWith(".aql")) {
            Reader r = new InputStreamReader(new FileInputStream(aql), "UTF-8");
            aql = Streams.copyToString(r);
            r.close();
        }
        this.aql = aql;
        return this;
    }

    public String getAQL() {
        return aql;
    }

    public AQLCommand setParameters(List<Object> params) {
        this.params = params;
        return this;
    }

    public List<Object> getParameters() {
        return params;
    }

    public AQLCommand setCallable(boolean callable) {
        this.callable = callable;
        return this;
    }

    public boolean isCallable() {
        return callable;
    }

    public AQLCommand setWrite(boolean write) {
        this.write = write;
        return this;
    }

    public boolean isWrite() {
        return write;
    }

    public boolean isQuery() {
        if (aql == null) {
            throw new IllegalArgumentException("no AQL found");
        }
        if (write) {
            return false;
        }
        if (STATEMENT_PATTERN.matcher(aql).find()) {
            return false;
        }
        int p1 = aql.toLowerCase().indexOf("select");
        if (p1 < 0) {
            return false;
        }
        int p2 = aql.toLowerCase().indexOf("update");
        if (p2 < 0) {
            return true;
        }
        int p3 = aql.toLowerCase().indexOf("insert");
        return p3 < 0 || p1 < p2 && p1 < p3;
    }

    /**
     * A register is for parameters of a callable statement.
     *
     * @param register a map for registering parameters
     */
    public void setRegister(Map<String, Object> register) {
        this.register = register;
    }

    /**
     * Get the parameters of a callable statement
     *
     * @return the register map
     */
    public Map<String, Object> getRegister() {
        return register;
    }

    @SuppressWarnings({"unchecked"})
    public static List<AQLCommand> parse(Map<String, Object> settings) {
        List<AQLCommand> aql = new LinkedList<AQLCommand>();
        if (!XContentMapValues.isArray(settings.get("aql"))) {
            settings.put("aql", Arrays.asList(settings.get("aql")));
        }
        List<Object> list = (List<Object>) settings.get("aql");
        for (Object entry : list) {
            AQLCommand command = new AQLCommand();
            try {
                if (entry instanceof Map) {
                    Map<String, Object> m = (Map<String, Object>) entry;
                    if (m.containsKey("statement")) {
                        command.setAQL((String) m.get("statement"));
                    }
                    if (m.containsKey("parameter")) {
                        command.setParameters(XContentMapValues.extractRawValues("parameter", m));
                    }
                    if (m.containsKey("write")) {
                        command.setWrite(XContentMapValues.nodeBooleanValue(m.get("write")));
                    }
                    if (m.containsKey("callable")) {
                        command.setCallable(XContentMapValues.nodeBooleanValue(m.get("callable")));
                    }
                    if (m.containsKey("register")) {
                        command.setRegister(XContentMapValues.nodeMapValue(m.get("register"), null));
                    }
                } else if (entry instanceof String) {
                    command.setAQL((String) entry);
                }
                aql.add(command);
            } catch (IOException e) {
                throw new IllegalArgumentException("AQL command not found", e);
            }
        }
        return aql;
    }

    public String toString() {
        return "statement=" + aql + " parameter=" + params + " write=" + write + " callable=" + callable;
    }

}
