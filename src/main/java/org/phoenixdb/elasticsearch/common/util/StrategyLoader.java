
package org.phoenixdb.elasticsearch.common.util;

import org.phoenixdb.elasticsearch.jdbc.strategy.Context;
import org.phoenixdb.elasticsearch.jdbc.strategy.JDBCSource;
import org.phoenixdb.elasticsearch.jdbc.strategy.Sink;
import org.phoenixdb.elasticsearch.jdbc.strategy.Source;
import org.phoenixdb.elasticsearch.jdbc.strategy.standard.StandardContext;
import org.phoenixdb.elasticsearch.jdbc.strategy.standard.StandardSink;
import org.phoenixdb.elasticsearch.jdbc.strategy.standard.StandardSource;

import java.util.ServiceLoader;

/**
 * The strategy loader
 */
public class StrategyLoader {

    /**
     * A context encapsulates the move from source to sink
     *
     * @param strategy the strategy
     * @return a context, or the StandardContext
     */
    public static Context newContext(String strategy) {
        ServiceLoader<Context> loader = ServiceLoader.load(Context.class);
        for (Context context : loader) {
            if (strategy.equals(context.strategy())) {
                return context.newInstance();
            }
        }
        return new StandardContext();
    }

    /**
     * Load a new source
     *
     * @param strategy the strategy
     * @return a source, or the StandardSource
     */
    public static Source newSource(String strategy) {
        ServiceLoader<Source> loader = ServiceLoader.load(Source.class);
        for (Source source : loader) {
            if (strategy.equals(source.strategy())) {
                return source.newInstance();
            }
        }
        return new StandardSource();
    }


    /**
     * Load a new JDBC source
     *
     * @param strategy the strategy
     * @return a source, or the StandardSource
     */
    public static JDBCSource newJDBCSource(String strategy) {
        ServiceLoader<JDBCSource> loader = ServiceLoader.load(JDBCSource.class);
        for (JDBCSource source : loader) {
            if (strategy.equals(source.strategy())) {
                return source.newInstance();
            }
        }
        return new StandardSource();
    }

    /**
     * A sink is the Elasticsearch side where the bulk processor lives
     *
     * @param strategy the strategy
     * @return a new instance of a sink, or an instance of the StandardSinkif strategy does not exist
     */
    public static Sink newSink(String strategy) {
        ServiceLoader<Sink> loader = ServiceLoader.load(Sink.class);
        for (Sink sink : loader) {
            if (strategy.equals(sink.strategy())) {
                return sink.newInstance();
            }
        }
        return new StandardSink();
    }

}
