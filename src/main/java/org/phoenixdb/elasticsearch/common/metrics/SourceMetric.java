
package org.phoenixdb.elasticsearch.common.metrics;

import org.elasticsearch.common.metrics.CounterMetric;
import org.joda.time.DateTime;

import java.util.concurrent.atomic.AtomicInteger;

public class SourceMetric {

    private long started;

    private final CounterMetric totalRows = new CounterMetric();

    private final CounterMetric totalSizeInBytes = new CounterMetric();

    private final CounterMetric succeeded = new CounterMetric();

    private final CounterMetric failed = new CounterMetric();

    private final AtomicInteger counter = new AtomicInteger();

    private CounterMetric currentRows = new CounterMetric();

    public CounterMetric getTotalRows() {
        return totalRows;
    }

    public CounterMetric getTotalSizeInBytes() {
        return totalSizeInBytes;
    }

    public void resetCurrentRows() {
        currentRows = new CounterMetric();
    }

    public CounterMetric getCurrentRows() {
        return currentRows;
    }

    public CounterMetric getSucceeded() {
        return succeeded;
    }

    public CounterMetric getFailed() {
        return failed;
    }

    public SourceMetric start() {
        this.started = System.nanoTime();
        return this;
    }

    public long elapsed() {
        return System.nanoTime() - started;
    }

    private DateTime lastExecutionStart;

    public void setLastExecutionStart(DateTime dateTime) {
       this.lastExecutionStart = dateTime;
    }

    public DateTime getLastExecutionStart() {
        return lastExecutionStart;
    }

    private DateTime lastExecutionEnd;

    public void setLastExecutionEnd(DateTime dateTime) {
        this.lastExecutionEnd = dateTime;
    }

    public DateTime getLastExecutionEnd() {
        return lastExecutionEnd;
    }

    public void setCounter(int counter) {
        this.counter.getAndSet(counter);
    }

    public int getCounter() {
        return counter.get();
    }

    public void incCounter() {
        counter.incrementAndGet();
    }

}