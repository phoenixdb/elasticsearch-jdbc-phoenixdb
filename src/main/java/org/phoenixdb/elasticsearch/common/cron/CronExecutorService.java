
package org.phoenixdb.elasticsearch.common.cron;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Executor service that schedules a task for execution via a cron expression.
 */
public interface CronExecutorService extends ExecutorService {
    /**
     * Schedules the specified task to execute according to the specified cron expression.
     *
     * @param task       the Runnable task to schedule
     * @param expression a cron expression
     * @return a future
     */
    Future<?> schedule(Runnable task, CronExpression expression);
}
