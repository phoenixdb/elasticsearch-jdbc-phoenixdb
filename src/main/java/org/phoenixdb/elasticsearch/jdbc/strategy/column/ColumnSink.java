
package org.phoenixdb.elasticsearch.jdbc.strategy.column;

import org.phoenixdb.elasticsearch.jdbc.strategy.standard.StandardSink;

public class ColumnSink extends StandardSink {

    @Override
    public String strategy() {
        return "column";
    }

    @Override
    public ColumnSink newInstance() {
        return new ColumnSink();
    }

}
