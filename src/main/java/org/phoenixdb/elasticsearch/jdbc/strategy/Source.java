
package org.phoenixdb.elasticsearch.jdbc.strategy;

import org.phoenixdb.elasticsearch.common.metrics.SourceMetric;

import java.io.IOException;

public interface Source<C extends Context> {

    /**
     * The strategy this source supports.
     *
     * @return the strategy as a string
     */
    String strategy();

    /**
     * Create new source instance
     *
     * @return a new source instance
     */
    Source<C> newInstance();

    /**
     * Set the context
     *
     * @param context the context
     * @return this source
     */
    Source<C> setContext(C context);

    C getContext();

    /**
     * Executed before fetch() is executed
     *
     * @throws Exception when execution fails
     */
    void beforeFetch() throws Exception;

    /**
     * Fetch a data portion from the database and pass it to the task
     * for further processing.
     *
     * @throws Exception when execution gives an error
     */
    void fetch() throws Exception;

    /**
     * Executed after fetch() has been executed or threw an exception.
     *
     * @throws Exception when execution fails
     */
    void afterFetch() throws Exception;

    /**
     * Shutdown source
     *
     * @throws IOException when shutdown fails
     */
    void shutdown() throws IOException;

    SourceMetric getMetric();
}
