
package org.phoenixdb.elasticsearch.jdbc.strategy;

import org.elasticsearch.common.settings.Settings;

public interface Context<S extends Source, T extends Sink> {

    enum State { BEFORE_FETCH, FETCH, AFTER_FETCH, IDLE, EXCEPTION }

    String strategy();

    Context newInstance();

    /**
     * Set the settings
     *
     * @param settings the settings
     * @return this context
     */
    Context setSettings(Settings settings);

    /**
     * Get the settings
     *
     * @return the settings
     */
    Settings getSettings();

    /**
     * Set source
     *
     * @param source the source
     * @return this context
     */
    Context setSource(S source);

    /**
     * Get source
     *
     * @return the source
     */
    S getSource();

    /**
     * Set sink
     *
     * @param sink the sink
     * @return this context
     */
    Context setSink(T sink);

    /**
     * Get sink
     *
     * @return the sink
     */
    T getSink();

    void execute() throws Exception;

    void beforeFetch() throws Exception;

    void fetch() throws Exception;

    void afterFetch() throws Exception;

    State getState();

    void log();

    void shutdown();
}
